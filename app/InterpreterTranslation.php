<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterpreterTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['content'];
}
