<?php

namespace App\Http\Controllers;

use App\Http\Requests\InterpreterRequest;
use App\Interpreter;
use Illuminate\Http\Request;

class InterpretersController extends Controller
{
    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $message = __('messages.laravel');
        $locale = app()->getLocale();
        return view('interpreters.index', ['locale' => $locale, 'message' => $message, 'interpreters' => Interpreter::all()]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $interpreter = Interpreter::findOrFail($id);
        return view('interpreters.show', compact('interpreter'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('interpreters.create');
    }

    /**
     * @param InterpreterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(InterpreterRequest $request)
    {
        $interpreter = new Interpreter();
        $locale = $request->session()->get('locale');
        $interpreter->translateOrNew($locale)->content = $request->get('content');
        $interpreter->user_id = $request->user()->id;
        $interpreter->save();
        return redirect(route('interpreters.index'));
    }

    /**
     * @param Interpreter $interpreter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Interpreter $interpreter)
    {
        return view('interpreters.edit', compact('interpreter'));
    }
    /**
     * @param InterpreterRequest $request
     * @param Interpreter $interpreter
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(InterpreterRequest $request, Interpreter $interpreter)
    {
        $interpreter->hasTranslation();
        $interpreter->update($request->all());
        return redirect(route('interpreters.index'));
    }

    /**
     * @param InterpreterRequest $request
     * @param Interpreter $interpreter
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(InterpreterRequest $request, Interpreter $interpreter)
    {
        $interpreter = Interpreter::findOrFail($interpreter);
        $interpreter->delete();

        return redirect(route('interpreters.index'));
    }

}
