@extends('layouts.app')

@section('content')
    <div class="col-12">
        <h1>{{ __('Create new word')}}</h1>
    </div>
    <div class="col-12">
        <form method="post" action="{{route('interpreters.store')}}">
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">{{ __('Content')}}</label>
                <textarea
                    name="content"
                    class="form-control"
                    id="exampleInputPassword1"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">{{ __('Create a word')}}</button>
        </form>
    </div>
    <div style="padding-top: 25px" class="col-2">
        <a href="{{url('/')}}" class="btn btn-primary btn-sm
active" role="button"
           aria-pressed="true">{{ __('Back')}}</a>
    </div>
@endsection
