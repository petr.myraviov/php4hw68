@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <blockquote class="blockquote">
            <div class="container">
                <div class="row-cols-4">
                    <div class="">
                       @if(!empty($interpreter->translate('ru')->content))
                            <label for="content"> {{ __('Russian')}}: {{$interpreter->translate('ru')->content}}</label><br>
                        @endif
                        @if(!empty($interpreter->translate('en')->content))
                            <label for="content"> {{ __('English')}}: {{$interpreter->translate('en')->content}}</label>
                        @endif
                    </div>
                    <footer class="blockquote-footer">
                        Author: {{$interpreter->user->email}}, created in
                        <cite title="Created at article">
                            {{$interpreter->created_at->diffForHumans()}}
                        </cite>
                    </footer>
                </div>
                <a href="{{url('/')}}" class="btn btn-primary btn-sm
  active" role="button" aria-pressed="true">{{ __('Back')}}</a>
            </div>

        </blockquote>
    </div>
@endsection
