@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <blockquote class="blockquote">
            <p class="mb-2" style="text-align: justify">

            </p>
            <div class="container">
                <div class="row-cols-4">
                        <form action="{{route('interpreters.update', ['interpreter' => $interpreter])}}" method="post">
                            @if(!empty($interpreter->translate('ru')->content))
                                <label for="content"> {{ __('Russian')}}: {{$interpreter->translate('ru')->content}}</label><br>
                            @elseif(!empty($interpreter->translate('en')->content))
                                <label for="content"> {{ __('English')}}: {{$interpreter->translate('en')->content}}</label>
                            @endif
                            <input type="text" name="content" value="{{$interpreter->content}}"/>
                            @csrf
                            @method('put')
                            <button style="margin-top: 10px" type="submit" class="btn btn-primary">{{ __('Update')}}</button>
                        </form>
                    <footer class="blockquote-footer">
                        Author: {{$interpreter->user->email}}, created in
                        <cite title="Created at article">
                            {{$interpreter->created_at->diffForHumans()}}
                        </cite>
                    </footer>
                </div>
                <a href="{{url('/')}}" class="btn btn-primary btn-sm
  active" role="button" aria-pressed="true">{{ __('Back')}}</a>
            </div>
        </blockquote>
    </div>
    </div>
    </div>
@endsection
