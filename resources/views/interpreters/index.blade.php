@extends('layouts.app')
@section('content')
    <div class="container">
    <div class="col-6">
        <div class="row">
            <div class="col-2">
                <h3>{{ __('Translator')}}</h3>
            </div>
        </div>
        @if (Auth::check())
            <div class="col-4">
                <a href="{{route('interpreters.create')}}" class="btn btn-primary btn-sm
  active" role="button" aria-pressed="true">{{ __('Create a new translation')}}</a>
            </div>
        @endif
    </div>
    <div class="row">
        @csrf
        @foreach($interpreters as $interpreter)
                      <div class="container">
                          <div class="col-md-3">
                               @if(!empty( $interpreter->content))
                                  <a href="{{route('interpreters.show', ['interpreter' =>
                                    $interpreter])}}" class="card-link"> {{ $interpreter->content }}</a>
                                  <a href="{{route('interpreters.edit', ['interpreter' => $interpreter])}}" class="card-link">{{ __('Edit')}}</a>
                                @endif
                          </div>
                      </div>
        @endforeach
    </div>
    </div>
@endsection
