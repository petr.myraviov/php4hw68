$(document).ready(function () {
    $('.delete').on('click', function (event) {
        const interpreterId = $(this).attr('data-interpreter-id');
        const token = $('input[type=hidden]').val();
        const interpreter = $('#delete-interpreter-' + interpreterId);
        console.log('interpreter ====> ', interpreter);
        $.ajax({
            url: `interpreters/${interpreterId}`,
            method: 'delete',
            data: {_token: token}
        })
            .done(function(response) {
                console.log('response => ', response);
                $(interpreter).remove();
            })
            .fail(function(response) {
                console.log('fail response => ', response);
            });
    })
});
